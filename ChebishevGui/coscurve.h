#ifndef COSCURVE_H
#define COSCURVE_H

#include <curve.h>

class CosCurve : public Curve
{
private:
    void Compute();
public:
    CosCurve(long nodesNumber, double leftLimit, double rightLimit)
        :Curve(nodesNumber, leftLimit, rightLimit)
    {
        qwtCurve = new QwtPlotCurve("Cos");
        qwtCurve->setRenderHint(QwtPlotItem::RenderAntialiased);
        qwtCurve->setPen(QPen(Qt::blue, 3));
        Compute();
    }
};

#endif // COSCURVE_H
