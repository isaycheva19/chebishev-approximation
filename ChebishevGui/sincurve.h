#ifndef SINCURVE_H
#define SINCURVE_H

#include <curve.h>

class SinCurve : public Curve
{
private:
    void Compute();
public:
    SinCurve(long nodesNumber, double leftLimit, double rightLimit)
        :Curve(nodesNumber, leftLimit, rightLimit)
    {
        qwtCurve = new QwtPlotCurve("Sin");
        qwtCurve->setRenderHint(QwtPlotItem::RenderAntialiased);
        qwtCurve->setPen(QPen(Qt::black, 3));
        Compute();
    }
};

#endif // SINCURVE_H
