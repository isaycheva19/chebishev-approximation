#ifndef EXCURVE_H
#define EXCURVE_H

#include <curve.h>

class ExCurve : public Curve
{
private:
    void Compute();
public:
    ExCurve(long nodesNumber, double leftLimit, double rightLimit)
        :Curve(nodesNumber, leftLimit, rightLimit)
    {
        qwtCurve = new QwtPlotCurve("Ex");
        qwtCurve->setRenderHint(QwtPlotItem::RenderAntialiased);
        qwtCurve->setPen(QPen(Qt::red, 3));
        Compute();
    }
};
#endif // EXCURVE_H
