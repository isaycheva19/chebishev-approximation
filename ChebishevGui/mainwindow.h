#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <qwt_plot_canvas.h>
#include <qwt_legend.h>
#include <qwt_plot_grid.h>
#include <qwt_symbol.h>
#include <sincurve.h>
#include <coscurve.h>
#include <x2curve.h>
#include <x3curve.h>
#include <excurve.h>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget* parent = 0);
    ~MainWindow();
public slots:
    void Redraw();
private:
    Ui::MainWindow* ui;

    QwtLegend* leg;
    QwtPlotGrid* grid;

    SinCurve* sin;
    CosCurve* cos;
    X2Curve* x2;
    X3Curve* x3;
    ExCurve* ex;
};

#endif // MAINWINDOW_H
