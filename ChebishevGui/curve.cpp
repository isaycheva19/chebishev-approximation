#include "curve.h"

Curve::Curve(long nodesNumber, double leftLimit, double rightLimit)
{
    this->nodesNumber = nodesNumber;
    x = (double*) malloc(nodesNumber * sizeof(double));
    y = (double*) malloc(nodesNumber * sizeof(double));

    //Handle left limit > right limit
    lftLim = leftLimit > rightLimit ? rightLimit : leftLimit;
    rigLim = leftLimit > rightLimit ? leftLimit : rightLimit;

    enabled = false;

    appromixatedCurve = new QwtPlotCurve();
}

void Curve::Toggle()
{
    enabled = !enabled;
}

void Curve::Attach(QwtPlot* plot)
{
    qwtCurve->attach(plot);
}

void Curve::Detach()
{
    qwtCurve->detach();
}

Curve::~Curve()
{
    free(x);
    free(y);

    delete qwtCurve;
    delete appromixatedCurve;
}
