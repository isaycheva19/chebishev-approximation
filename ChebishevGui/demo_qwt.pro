#-------------------------------------------------
#
# Project created by QtCreator 2010-08-17T09:04:07
#
#-------------------------------------------------

QT  += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET  = demo_qwt
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    curve.cpp \
    sincurve.cpp \
    coscurve.cpp \
    x2curve.cpp \
    x3curve.cpp \
    excurve.cpp

HEADERS += mainwindow.h \
    curve.h \
    sincurve.h \
    coscurve.h \
    x2curve.h \
    x3curve.h \
    excurve.h

FORMS   += mainwindow.ui


include(qwt.pri)

