#ifndef CURVE_H
#define CURVE_H

#include <stdlib.h>
#include <QObject>
#include <qwt_plot_curve.h>

class Curve : public QObject
{
    Q_OBJECT
protected:
    bool enabled;
    double* x;
    double* y;
    long nodesNumber;
    QwtPlotCurve* qwtCurve;
    //This is approximated curve. It should be evaluated when Compute method is called
    QwtPlotCurve* appromixatedCurve;
    double lftLim;
    double rigLim;

    virtual void Compute() = 0;
public slots:
    void Toggle();
public:
    Curve(long nodesNumber, double leftLimit, double rightLimit);

    inline long GetNodesNumber() const { return nodesNumber; }
    inline const double* GetXData() const { return x; }
    inline const double* GetYData() const { return y; }
    inline bool IsEnabled() const { return enabled; }

    void Attach(QwtPlot* plot);
    void Detach();

    virtual ~Curve();
};

#endif // CURVE_H
