#include "coscurve.h"

void CosCurve::Compute()
{
   {
        double step = (rigLim - lftLim) / (nodesNumber - 1);
        double xVal = lftLim;
        long cntr = 0;
        for (; cntr < nodesNumber;)
        {
            x[cntr] = xVal;
            y[cntr] = cos(x[cntr]);
            xVal += step;
            ++cntr;
        }
    }

#if QWT_VERSION < 0x060000
    qwtCurve->setData(x, y, nodesNumber);
#else
    qwtCurve->setSamples(x, y, nodesNumber);
#endif
}
