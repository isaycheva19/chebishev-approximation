#ifndef X3CURVE_H
#define X3CURVE_H

#include <curve.h>

class X3Curve : public Curve
{
private:
    void Compute();
public:
    X3Curve(long nodesNumber, double leftLimit, double rightLimit)
        :Curve(nodesNumber, leftLimit, rightLimit)
    {
        qwtCurve = new QwtPlotCurve("X3");
        qwtCurve->setRenderHint(QwtPlotItem::RenderAntialiased);
        qwtCurve->setPen(QPen(Qt::green, 3));
        Compute();
    }
};

#endif // X3CURVE_H
