#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    sin = new SinCurve(10, -1, 1);
    cos = new CosCurve(10, -1, 1);
    x2 = new X2Curve(10, -1, 1);
    x3 = new X3Curve(10, -1, 1);
    ex = new ExCurve(10, -1, 1);

    QObject::connect(ui->buildPushBtn, SIGNAL(pressed()), this, SLOT(Redraw()));
    QObject::connect(ui->sinCheckBox, SIGNAL(pressed()), sin, SLOT(Toggle()));
    QObject::connect(ui->cosCheckBox, SIGNAL(pressed()), cos, SLOT(Toggle()));
    QObject::connect(ui->x2CheckBox, SIGNAL(pressed()), x2, SLOT(Toggle()));
    QObject::connect(ui->x3CheckBox, SIGNAL(pressed()), x3, SLOT(Toggle()));
    QObject::connect(ui->exCheckBox, SIGNAL(pressed()), ex, SLOT(Toggle()));

    grid = new QwtPlotGrid();
    grid->enableXMin(true);
    grid->setMajPen(QPen(Qt::black,0,Qt::SolidLine));
    grid->setMinPen(QPen(Qt::gray,0,Qt::SolidLine));

    grid->attach(ui->Plot);

    ui->Plot->setAxisAutoScale(QwtPlot::yLeft);
    ui->Plot->setAxisAutoScale(QwtPlot::yRight);
    ui->Plot->setAxisScale(QwtPlot::xBottom,-1.25,1.25);
}

void MainWindow::Redraw()
{
    if (sin->IsEnabled())
        sin->Attach(ui->Plot);
    else
        sin->Detach();

    if (cos->IsEnabled())
        cos->Attach(ui->Plot);
    else
        cos->Detach();

    if (x2->IsEnabled())
        x2->Attach(ui->Plot);
    else
        x2->Detach();

    if (x3->IsEnabled())
        x3->Attach(ui->Plot);
    else
        x3->Detach();

    if (ex->IsEnabled())
        ex->Attach(ui->Plot);
    else
        ex->Detach();

    ui->Plot->replot();
}

MainWindow::~MainWindow()
{
    delete grid;

    delete sin;
    delete cos;
    delete x2;
    delete x3;
    delete ex;

    delete ui;
}
