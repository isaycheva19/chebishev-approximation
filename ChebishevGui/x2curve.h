#ifndef X2CURVE_H
#define X2CURVE_H

#include <curve.h>

class X2Curve : public Curve
{
private:
    void Compute();
public:
    X2Curve(long nodesNumber, double leftLimit, double rightLimit)
        :Curve(nodesNumber, leftLimit, rightLimit)
    {
        qwtCurve = new QwtPlotCurve("X2");
        qwtCurve->setRenderHint(QwtPlotItem::RenderAntialiased);
        qwtCurve->setPen(QPen(Qt::yellow, 3));
        Compute();
    }
};

#endif // X2CURVE_H
