#include "trapezeMethod.h"
#include "math.h"

int ComputeIntegral(const FunctionData_t* const func, double* result)
{
    if (func->lftLim > func->rigLim)
    {
        result = NULL;
        return TRAPEZE_LIMITS_ERROR;
    }

    double step = (func->rigLim - func->lftLim) / (func->dataLen - 1);
    double sum = step * func->data[0] / 2 + step * func->data[func->dataLen - 1] / 2;

    long i = 1;
    for (; i < func->dataLen - 1 ;)
    {
        sum += step * func->data[i];
        ++i;
    }

    *result = sum;
    return TRAPEZE_OK;
}
