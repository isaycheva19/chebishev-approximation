#ifndef TRAPEZEMETHOD_H
#define TRAPEZEMETHOD_H

#include <stdlib.h>

#define TRAPEZE_OK 0
#define TRAPEZE_LIMITS_ERROR 1

#ifdef __cplusplus
extern "C"
{
#endif

typedef struct FunctionData_s
{
    double* data;
    long dataLen;
    double lftLim;
    double rigLim;
} FunctionData_t;

int ComputeIntegral(const FunctionData_t* const func, double* result);

#ifdef __cplusplus
}
#endif

#endif // TRAPEZEMETHOD_H
