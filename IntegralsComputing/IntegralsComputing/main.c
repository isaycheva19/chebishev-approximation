#include <stdio.h>
#include <math.h>
#include "trapezeMethod.h"

#define DISCRETEZATION 100
#define FUNC(x) cos(x)
#define LEFT_LIMIT -1 * 2 * M_PI
#define RIGHT_LIMIT 2 * M_PI

int main(void)
{
    FunctionData_t* func = (FunctionData_t*) malloc(sizeof(FunctionData_t));
    func->data = (double*) malloc(DISCRETEZATION * sizeof(double));

    func->dataLen = DISCRETEZATION;
    func->lftLim = LEFT_LIMIT;
    func->rigLim = RIGHT_LIMIT;

    double i = func->lftLim;
    int cntr = 0;
    float step = func->rigLim / (DISCRETEZATION - 1);

    for ( ; i <= func->rigLim ; )
    {
        func->data[cntr++] = FUNC(i);
        i += step;
    }

    double integralValue = 0;
    int error = ComputeIntegral(func, &integralValue);
    if (error != TRAPEZE_OK)
    {
        printf("Some fucking error, check limits");
        return 1;
    }

    free(func->data);
    free(func);
    return 0;
}

